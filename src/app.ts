import { config } from 'dotenv'
import express from 'express'

import { apiRouter } from './routes/api/ApiRouter'

config()

const port = process.env.PORT

export const app = express()

app.use('/api', apiRouter)

app.get('/', (req, res) => {
  res.json({ message: 'success' })
})
app.get('/liveness', (req, res) => {
  res.json({ message: 'success' })
})

app.listen(port, () => console.log(`API Server start at port ${port}`))
