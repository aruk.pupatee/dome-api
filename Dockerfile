FROM node:12-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn --production=true

COPY . .

EXPOSE 3000

CMD ["yarn" , "start"]
